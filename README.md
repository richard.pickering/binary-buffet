# Binary Buffet

A simple reflective Java binary parsing/decoding and serialisation library.

## Terms

Binary Buffet uses the terms hydration and dessication to describe deserialisation and serialisation.

### Hydration

Hydration is 'hydrating' binary data into rich Java POJOs.

### Dessication

Dessication is 'dessicating' rich Java POJOs into binary data (byte arrays).

## Example

Please see the integration tests of Binary Buffet for an example of use.
