package uk.co.richardpickering.binarybuffet.reflection;

import org.junit.Test;
import uk.co.richardpickering.binarybuffet.FieldDessicationConversion;
import uk.co.richardpickering.binarybuffet.annotation.BinaryField;
import uk.co.richardpickering.binarybuffet.reflection.exception.BinaryBuffetConfigException;

import java.lang.reflect.Field;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class BinaryFieldDataExtractorTest {

    private final BinaryFieldDataExtractor extractor = new BinaryFieldDataExtractor();

    @Test
    public void canConvertValidBinaryFieldAnnotation() {

        Map<Field, BinaryFieldData> result = extractor.extract(ValidClass.class);

        assertThat(result).isNotEmpty();
    }

    @Test
    public void cannotConvertIfDessicationCannotBeInstantiated() {

        assertThatThrownBy(() -> extractor.extract(InvalidClass.class))
                .isInstanceOf(BinaryBuffetConfigException.class)
                .hasMessage("'INVALID_CUSTOM_DESSICATION' - Could not instantiate custom conversion as type 'class uk.co.richardpickering.binarybuffet.reflection.BinaryFieldDataExtractorTest$AbstractDessication' has no default declared constructor");
    }

    private static class ValidClass {
        @BinaryField(offset = 0, width = 4, dessication = ValidDessication.class)
        private Integer field;
    }

    public static class ValidDessication implements FieldDessicationConversion {

        @Override
        public byte[] convert(final Object value) {
            return new byte[0];
        }
    }

    private static class InvalidClass {
        @BinaryField(offset = 0, width = 4, dessication = AbstractDessication.class)
        private Integer field;
    }

    private abstract class AbstractDessication implements FieldDessicationConversion {

        private AbstractDessication() {

        }

        @Override
        public byte[] convert(final Object value) {
            return new byte[0];
        }
    }
}