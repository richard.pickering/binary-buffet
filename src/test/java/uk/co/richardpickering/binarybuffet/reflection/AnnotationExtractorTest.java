package uk.co.richardpickering.binarybuffet.reflection;

import org.junit.Test;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.Map;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.assertj.core.api.Assertions.assertThat;

public class AnnotationExtractorTest {

    private final AnnotationExtractor annotationExtractor = new AnnotationExtractor();

    @Test
    public void classLevelAnnotations() {

        assertThat(annotationExtractor.getClassLevelAnnotation(ClassWithoutAnnotation.class, ClassAnnotation.class))
                .isNotPresent();

        assertThat(annotationExtractor.getClassLevelAnnotation(ClassWithAnnotation.class, ClassAnnotation.class))
                .isPresent();
    }

    @Test
    public void fieldLevelAnnotations() {

        Map<Field, FieldAnnotation> result = annotationExtractor.getFieldAnnotations(ClassWithFieldAnnotation.class, FieldAnnotation.class);

        assertThat(result.values().stream().map(FieldAnnotation::value)).containsExactlyInAnyOrder(10, 20);
    }

    @Retention(RUNTIME)
    @Target(TYPE)
    public @interface ClassAnnotation {

    }

    public static class ClassWithoutAnnotation {

    }

    @ClassAnnotation
    public static class ClassWithAnnotation {

    }

    @Retention(RUNTIME)
    @Target(FIELD)
    @interface FieldAnnotation {
        int value();
    }

    public static class ClassWithFieldAnnotation {
        @FieldAnnotation(10)
        private String field1;

        @FieldAnnotation(20)
        private String field2;
    }
}