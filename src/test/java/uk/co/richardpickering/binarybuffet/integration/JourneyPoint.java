package uk.co.richardpickering.binarybuffet.integration;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import uk.co.richardpickering.binarybuffet.annotation.BinaryField;
import uk.co.richardpickering.binarybuffet.annotation.SubByteField;
import uk.co.richardpickering.binarybuffet.dessication.Dessicatable;
import uk.co.richardpickering.binarybuffet.hydration.Hydratable;

public class JourneyPoint implements Dessicatable, Hydratable {

    /**
     * The identifier of the record.
     */
    @BinaryField(offset = 0, width = 1)
    private short identifier;

    /**
     * Epoch seconds
     */
    @BinaryField(offset = 1, width = 4)
    private int timestamp;

    /**
     * Latitude of a journey point - DMM format (DDMMM.MMMM)
     */
    @BinaryField(offset = 5, width = 4)
    private int latitude;

    /**
     * Longitude of a journey point - DMM format (DDMMM.MMMM)
     */
    @BinaryField(offset = 9, width = 4)
    private int longitude;

    /**
     * Bearing of the journey point in whole degrees.
     */
    @BinaryField(offset = 13, width = 2)
    private int bearing;

    /**
     * Whole number of KPH
     */
    @BinaryField(offset = 15, width = 1)
    private int speedInKph;

    /**
     * Semi-Hemisphere (i.e. Quadrant of the Earth) - to infer sign of lat/lon co-ordinates.
     * 0 = North West
     * 1 = North East
     * 2 = South West
     * 3 = South East
     */
    @BinaryField(offset = 16, width = 1)
    private int semiHemisphere;

    @SubByteField(byteOffset = 17, bitOffset = 0, bitWidth = 2)
    private int fixQuality;

    @SubByteField(byteOffset = 17, bitOffset = 2, bitWidth = 6)
    private int somethingElse;

    public JourneyPoint() {

    }

    JourneyPoint(final short identifier,
                 final int timestamp,
                 final int latitude,
                 final int longitude,
                 final int bearing,
                 final int speedInKph,
                 final int semiHemisphere,
                 final int fixQuality,
                 final int somethingElse) {

        this.identifier = identifier;
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.bearing = bearing;
        this.speedInKph = speedInKph;
        this.semiHemisphere = semiHemisphere;
        this.fixQuality = fixQuality;
        this.somethingElse = somethingElse;
    }

    @Override
    public int getWidth() {
        return 17;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JourneyPoint that = (JourneyPoint) o;
        return Objects.equal(identifier, that.identifier) &&
                Objects.equal(timestamp, that.timestamp) &&
                Objects.equal(latitude, that.latitude) &&
                Objects.equal(longitude, that.longitude) &&
                Objects.equal(bearing, that.bearing) &&
                Objects.equal(speedInKph, that.speedInKph) &&
                Objects.equal(semiHemisphere, that.semiHemisphere);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(identifier, timestamp, latitude, longitude, bearing, speedInKph, semiHemisphere);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("identifier", identifier)
                .add("timestamp", timestamp)
                .add("latitude", latitude)
                .add("longitude", longitude)
                .add("bearing", bearing)
                .add("speedInKph", speedInKph)
                .add("semiHemisphere", semiHemisphere)
                .toString();
    }
}
