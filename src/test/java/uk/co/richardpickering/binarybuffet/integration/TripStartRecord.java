package uk.co.richardpickering.binarybuffet.integration;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import uk.co.richardpickering.binarybuffet.annotation.BinaryField;
import uk.co.richardpickering.binarybuffet.dessication.Dessicatable;
import uk.co.richardpickering.binarybuffet.hydration.Hydratable;

public class TripStartRecord implements Hydratable, Dessicatable {

    @BinaryField(offset = 0, width = 1)
    private Short identifier;

    @BinaryField(offset = 1, width = 17)
    private String vin;

    private TripStartRecord() {

    }

    public TripStartRecord(final short identifier, final String vin) {
        this.identifier = identifier;
        this.vin = vin;
    }

    public Short getIdentifier() {
        return identifier;
    }

    public String getVin() {
        return vin;
    }

    @Override
    public int getWidth() {
        return 18;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripStartRecord that = (TripStartRecord) o;
        return Objects.equal(identifier, that.identifier) &&
                Objects.equal(vin, that.vin);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(identifier, vin);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("identifier", identifier)
                .add("vin", vin)
                .toString();
    }
}
