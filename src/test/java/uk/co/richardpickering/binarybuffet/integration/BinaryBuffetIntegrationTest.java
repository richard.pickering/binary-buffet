package uk.co.richardpickering.binarybuffet.integration;

import org.junit.Test;
import uk.co.richardpickering.binarybuffet.BinaryBuffet;
import uk.co.richardpickering.binarybuffet.dessication.BinaryDataOutput;
import uk.co.richardpickering.binarybuffet.dessication.DessicationRecordPayload;
import uk.co.richardpickering.binarybuffet.dessication.Dessicator;
import uk.co.richardpickering.binarybuffet.dessication.extraction.conversions.FieldConverter;
import uk.co.richardpickering.binarybuffet.hydration.BinaryDataInput;
import uk.co.richardpickering.binarybuffet.hydration.BinaryDataInput.InputStreamBinaryInput;
import uk.co.richardpickering.binarybuffet.hydration.HydrationResult;
import uk.co.richardpickering.binarybuffet.hydration.RecordHydrator;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BinaryBuffetIntegrationTest {

    private static final Byte[] BOXED_RECORD_DATA = { (byte) 1, (byte) 92, (byte) 42, (byte) -83, (byte) -128, (byte) 31, (byte) -70,
            (byte) 51, (byte) 34, (byte) 0, (byte) -61, (byte) 88, (byte) -52, (byte) 0, (byte) 6, (byte) 52, (byte) 1 };

    private static final byte[] UNBOXED_RECORD_DATA = { (byte) 1, (byte) 92, (byte) 42, (byte) -83, (byte) -128, (byte) 31, (byte) -70,
            (byte) 51, (byte) 34, (byte) 0, (byte) -61, (byte) 88, (byte) -52, (byte) 0, (byte) 6, (byte) 52, (byte) 1 };

    @Test
    public void hydratesAndDessicates() {

        RecordHydrator hydrator = BinaryBuffet.buildHydrator()
                .forIdentifier(0, (short) 1, JourneyPoint.class)
                .build();

        BinaryDataInput input = new InputStreamBinaryInput(new ByteArrayInputStream(UNBOXED_RECORD_DATA));

        HydrationResult result = hydrator.hydrate(input);

        Dessicator dessicator = BinaryBuffet.dessicator(FieldConverter.builder()
                .build());

        DessicationRecordPayload.Builder payloadBuilder = DessicationRecordPayload.builder();

        result.getRecordsOfType(JourneyPoint.class)
                .forEach(journeyPoint -> payloadBuilder.withRecord(journeyPoint, JourneyPoint.class));

        final List<Byte> outputBinary = new ArrayList<>();

        BinaryDataOutput output = new BinaryDataOutput() {
            @Override
            public void write(byte[] data) {
                for (final byte dataItem : data) {
                    outputBinary.add(dataItem);
                }
            }

            @Override
            public void close() {

            }
        };

        dessicator.dessicate(payloadBuilder.build(), output);

        assertThat(outputBinary).containsExactly(BOXED_RECORD_DATA);
    }
}
