package uk.co.richardpickering.binarybuffet.integration;

import org.junit.Before;
import org.junit.Test;
import uk.co.richardpickering.binarybuffet.dessication.BinaryDataOutput;
import uk.co.richardpickering.binarybuffet.dessication.DessicationRecordPayload;
import uk.co.richardpickering.binarybuffet.dessication.Dessicator;
import uk.co.richardpickering.binarybuffet.dessication.RecordDessicator;
import uk.co.richardpickering.binarybuffet.dessication.extraction.BinaryFieldExtractor;
import uk.co.richardpickering.binarybuffet.dessication.extraction.conversions.FieldConverter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class DessicationIntegrationTest {

    private static final Instant TIMESTAMP = LocalDateTime.of(2019, 1, 1, 0, 0).toInstant(UTC);

    /* Latitude is actually 53 22.96482 in DMM (middle of Sheffield) */
    private static final int LATITUDE = 532296482;

    /* Longitude is actually -1 28.02252 in DMM (middle of Sheffield)*/
    private static final int LONGITUDE = 12802252;

    private static final int BEARING = 6;

    private static final int SPEED = 52;

    private static final int SEMI_HEMISPHERE = 1;

    private Dessicator dessicator;

    @Before
    public void setup() {

        this.dessicator = new Dessicator(new RecordDessicator(new BinaryFieldExtractor(FieldConverter.builder()
                .build())));
    }

    @Test
    public void dessicatesJourneyPoint() {

        JourneyPoint journeyPoint = new JourneyPoint((short) 1, (int) TIMESTAMP.getEpochSecond(), LATITUDE, LONGITUDE, BEARING, SPEED,
                SEMI_HEMISPHERE, 1, 10);

        final List<Byte> outputBinary = new ArrayList<>();

        BinaryDataOutput output = new BinaryDataOutput() {
            @Override
            public void write(byte[] data) {
                for (final byte dataItem : data) {
                    outputBinary.add(dataItem);
                }
            }

            @Override
            public void close() {

            }
        };

        dessicator.dessicate(DessicationRecordPayload.builder()
                .withRecord(new TripStartRecord((short) 0, "ABCDEFGHIJKLMNOPQ"), TripStartRecord.class)
                .withRecord(journeyPoint, JourneyPoint.class)
                .build(), output);

        assertThat(outputBinary).containsExactly(
                /* Trip start record... */
                (byte) 0, (byte) 65, (byte) 66, (byte) 67, (byte) 68, (byte) 69, (byte) 70, (byte) 71, (byte) 72,
                (byte) 73, (byte) 74, (byte) 75, (byte) 76, (byte) 77, (byte) 78, (byte) 79, (byte) 80, (byte) 81,
                /* Journey record... */
                (byte) 1, (byte) 92, (byte) 42, (byte) -83, (byte) -128, (byte) 31, (byte) -70,
                (byte) 51, (byte) 34, (byte) 0, (byte) -61, (byte) 88, (byte) -52, (byte) 0, (byte) 6, (byte) 52, (byte) 1);
    }
}
