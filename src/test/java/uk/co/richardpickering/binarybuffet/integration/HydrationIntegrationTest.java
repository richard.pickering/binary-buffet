package uk.co.richardpickering.binarybuffet.integration;

import org.junit.Test;
import uk.co.richardpickering.binarybuffet.hydration.BinaryDataInput;
import uk.co.richardpickering.binarybuffet.hydration.BinaryDataInput.InputStreamBinaryInput;
import uk.co.richardpickering.binarybuffet.hydration.HydrationResult;
import uk.co.richardpickering.binarybuffet.hydration.RecordHydrator;

import java.io.ByteArrayInputStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class HydrationIntegrationTest {

    private static final byte[] RECORD_DATA = {
            /* Trip start record... */
            (byte) 0, (byte) 65, (byte) 66, (byte) 67, (byte) 68, (byte) 69, (byte) 70, (byte) 71, (byte) 72,
            (byte) 73, (byte) 74, (byte) 75, (byte) 76, (byte) 77, (byte) 78, (byte) 79, (byte) 80, (byte) 81,
            /* Journey record... */
            (byte) 1, (byte) 92, (byte) 42, (byte) -83, (byte) -128, (byte) 31, (byte) -70,
            (byte) 51, (byte) 34, (byte) 0, (byte) -61, (byte) 88, (byte) -52, (byte) 0, (byte) 6, (byte) 52, (byte) 1 };

    @Test
    public void hydrates() {

        RecordHydrator hydrator = RecordHydrator.builder()
                .forIdentifier(0, (short) 1, JourneyPoint.class)
                .forIdentifier(0, (short) 0, TripStartRecord.class)
                .build();

        BinaryDataInput input = new InputStreamBinaryInput(new ByteArrayInputStream(RECORD_DATA));

        HydrationResult result = hydrator.hydrate(input);

        List<JourneyPoint> points = result.getRecordsOfType(JourneyPoint.class);

        assertThat(points).containsExactly(new JourneyPoint((short) 1, 1546300800, 532296482, 12802252, 6, 52, 1, 1, 10));

        assertThat(result.getRecordsOfType(TripStartRecord.class)).containsExactly(new TripStartRecord((short) 0, "ABCDEFGHIJKLMNOPQ"));
    }
}
