package uk.co.richardpickering.binarybuffet.dessication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import uk.co.richardpickering.binarybuffet.annotation.BinaryField;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DessicatorTest {

    private static final DessicationRecordPayload RECORDS = DessicationRecordPayload.builder()
            .withRecord(new ExampleRecord((short) 0, 0), ExampleRecord.class)
            .withRecord(new ExampleRecord((short) 1, 10), ExampleRecord.class)
            .withRecord(new ExampleRecord((short) 2, 20), ExampleRecord.class)
            .build();

    @Mock
    private RecordDessicator recordDessicator;

    @Test
    public void dessicates() {

        byte[] expectedOutput = { 0, 1, 2, 3 };
        when(recordDessicator.dessicate(any())).thenReturn(expectedOutput);

        Dessicator dessicator = new Dessicator(recordDessicator);

        List<Byte> output = new ArrayList<>();

        dessicator.dessicate(RECORDS, new BinaryDataOutput() {
            @Override
            public void write(final byte[] data) {

                for (final byte item : data) {
                    output.add(item);
                }
            }

            @Override
            public void close() {

            }
        });

        assertThat(output).containsExactly((byte) 0, (byte) 1, (byte) 2, (byte) 3, (byte) 0, (byte) 1, (byte) 2,
                (byte) 3, (byte) 0, (byte) 1, (byte) 2, (byte) 3);
    }

    public static class ExampleRecord implements Dessicatable {

        @BinaryField(offset = 0, width = 1)
        private short recordType;

        @BinaryField(offset = 1, width = 4)
        private int timestamp;

        ExampleRecord(final short recordType, final int timestamp) {

            this.recordType = recordType;
            this.timestamp = timestamp;
        }

        @Override
        public int getWidth() {
            return 0;
        }
    }
}