package uk.co.richardpickering.binarybuffet.dessication.extraction.conversions;

import org.junit.Before;
import org.junit.Test;
import uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static uk.co.richardpickering.binarybuffet.ByteOrder.BIG_ENDIAN;

public class IntegerConverterTest {

    private IntegerConverter integerConverter;

    @Before
    public void setup() {
        this.integerConverter = new IntegerConverter(new IntegerByteExtractor(BIG_ENDIAN));
    }

    @Test
    public void convertsValidByteValue() {

        byte[] result = integerConverter.convert(BinaryFieldData.builder(0, (short) 1).build(), (byte) 0b00001111);

        assertThat(result).containsExactly(0b00001111);
    }

    @Test
    public void convertsValidShortValue() {

        byte[] result = integerConverter.convert(BinaryFieldData.builder(0, (short) 2).build(), (short) 0b11110000_00001111);

        assertThat(result).containsExactly(0b11110000, 0b00001111);
    }

    @Test
    public void convertsValidIntValue() {

        byte[] result = integerConverter.convert(BinaryFieldData.builder(0, (short) 4).build(), 0b11110000_00001111_11110000_00001111);

        assertThat(result).containsExactly(0b11110000, 0b00001111, 0b11110000, 0b00001111);
    }

    @Test
    public void convertsValidLongValue() {

        byte[] result = integerConverter.convert(BinaryFieldData.builder(0, (short) 8).build(), 0b11110000_00001111_11110000_00001111_11110000_00001111_11110000_00001111L);

        assertThat(result).containsExactly(0b11110000, 0b00001111, 0b11110000, 0b00001111, 0b11110000, 0b00001111, 0b11110000, 0b00001111);
    }

    @Test
    public void doesNotConvertValueNotAssignableToLong() {

        assertThatThrownBy(() -> integerConverter.convert(BinaryFieldData.builder(0, (short) 2).build(), "Something!"))
                .isInstanceOf(DessicationFailure.class)
                .hasMessage("'CONVERSION_ERROR' - Could not convert value 'Something!' to bytes using integer conversion - type 'class java.lang.String' not assignable from Byte.class, Short.class, Integer.class or Long.class.");
    }

}