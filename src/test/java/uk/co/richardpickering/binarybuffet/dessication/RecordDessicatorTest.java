package uk.co.richardpickering.binarybuffet.dessication;

import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import uk.co.richardpickering.binarybuffet.annotation.BinaryField;
import uk.co.richardpickering.binarybuffet.dessication.extraction.BinaryFieldExtractor;
import uk.co.richardpickering.binarybuffet.dessication.extraction.BinaryFieldExtractor.BinaryFieldValue;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RecordDessicatorTest {

    @Mock
    private BinaryFieldExtractor binaryFieldExtractor;

    private RecordDessicator recordDessicator;

    @Before
    public void setup() {
        this.recordDessicator = new RecordDessicator(binaryFieldExtractor);
    }

    @Test
    public void dessicatesRecordsAsExpected() {

        DessicationRecord<ExampleRecord> record = new DessicationRecord<>(ExampleRecord.class, new ExampleRecord(102));

        when(binaryFieldExtractor.extractBinaryFields(record))
                .thenReturn(ImmutableList.of(new BinaryFieldValue(new byte[] { 1, 2 }, 1, 3),
                        new BinaryFieldValue(new byte[] { 3, 4, 5 }, 3, 3)));

        byte[] bytes = recordDessicator.dessicate(record);

        assertThat(bytes).containsExactly(0, 1, 2, 3, 4, 5);
    }

    public static class ExampleRecord implements Dessicatable {

        @BinaryField(offset = 0, width = 4)
        private final int identifier;

        ExampleRecord(final int identifier) {
            this.identifier = identifier;
        }

        @Override
        public int getWidth() {
            return 4;
        }
    }
}