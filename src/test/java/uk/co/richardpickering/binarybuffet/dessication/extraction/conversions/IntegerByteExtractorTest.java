package uk.co.richardpickering.binarybuffet.dessication.extraction.conversions;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static uk.co.richardpickering.binarybuffet.ByteOrder.BIG_ENDIAN;
import static uk.co.richardpickering.binarybuffet.ByteOrder.LITTLE_ENDIAN;

public class IntegerByteExtractorTest {

    @Test
    public void extractsBytesForBigEndianShort() {

        IntegerByteExtractor extractor = new IntegerByteExtractor(BIG_ENDIAN);

        short value = (short) 0b11110000_00001111;

        assertThat(extractor.extract(value, (short) 2)).containsExactly(0b11110000, 0b00001111);
    }

    @Test
    public void extractsBytesForBigEndianInt() {

        IntegerByteExtractor extractor = new IntegerByteExtractor(BIG_ENDIAN);

        int value = 0b00111100_11000011_11110000_00001111;

        assertThat(extractor.extract(value, (short) 4)).containsExactly(0b00111100, 0b11000011, 0b11110000, 0b00001111);
    }

    @Test
    public void extractsBytesForLittleEndianShort() {

        IntegerByteExtractor extractor = new IntegerByteExtractor(LITTLE_ENDIAN);

        short value = (short) 0b11110000_00001111;

        assertThat(extractor.extract(value, (short) 2)).containsExactly(0b00001111, 0b11110000);
    }

    @Test
    public void extractsBytesForLittleEndianInt() {

        IntegerByteExtractor extractor = new IntegerByteExtractor(LITTLE_ENDIAN);

        int value = 0b00111100_11000011_11110000_00001111;

        assertThat(extractor.extract(value, (short) 4)).containsExactly(0b00001111, 0b11110000, 0b11000011, 0b00111100);
    }
}