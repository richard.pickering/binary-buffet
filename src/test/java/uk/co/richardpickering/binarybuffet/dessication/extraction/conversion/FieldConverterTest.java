package uk.co.richardpickering.binarybuffet.dessication.extraction.conversion;

import org.junit.Test;
import uk.co.richardpickering.binarybuffet.FieldDessicationConversion;
import uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure;
import uk.co.richardpickering.binarybuffet.dessication.extraction.conversions.FieldConverter;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class FieldConverterTest {

    private static final BinaryFieldData BINARY_FIELD_DATA = BinaryFieldData.builder(0, (short) 3).build();

    @Test
    public void convertsFieldsWithRegisteredConversions() {

        FieldConverter fieldConverter = FieldConverter.builder()
                .withConversion(Integer.class, (field, value) -> new byte[] { 0, 1, 2 })
                .build();

        assertThat(fieldConverter.convertField(BINARY_FIELD_DATA, Integer.class, 1234)).containsExactly(0, 1, 2);
    }

    @Test
    public void cannotConvertIfConversionNotRegistered() {

        FieldConverter fieldConverter = FieldConverter.builder()
                .build();

        assertThatThrownBy(() -> fieldConverter.convertField(BINARY_FIELD_DATA, LocalDateTime.class, 1234))
                .isInstanceOf(DessicationFailure.class)
                .hasMessage("'CONVERSION_ERROR' - Could not convert field - no registered conversion for type 'class java.time.LocalDateTime'");
    }

    @Test
    public void usesCustomDessicationConversionIfProvided() {

        FieldConverter fieldConverter = FieldConverter.builder()
                .withConversion(Integer.class, (field, value) -> new byte[] { 0, 1, 2 })
                .build();

        BinaryFieldData binaryFieldData = BinaryFieldData.builder(0, (short) 3)
                .withFieldDessicationConversion(new CustomConversion())
                .build();

        assertThat(fieldConverter.convertField(binaryFieldData, Integer.class, 1234)).containsExactly(5, 4, 3, 2, 1);
    }

    private static class CustomConversion implements FieldDessicationConversion {

        @Override
        public byte[] convert(final Object value) {
            return new byte[] { 5, 4, 3, 2, 1 };
        }
    }
}