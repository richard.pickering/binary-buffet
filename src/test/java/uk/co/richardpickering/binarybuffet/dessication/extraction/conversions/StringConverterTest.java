package uk.co.richardpickering.binarybuffet.dessication.extraction.conversions;

import org.junit.Test;
import uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class StringConverterTest {

    private final StringConverter converter = new StringConverter();

    @Test
    public void canConvertStrings() {

        byte[] bytes = converter.convert(BinaryFieldData.builder(0, (short) 15).build(), "This is a string");

        assertThat(new String(bytes, UTF_8)).isEqualTo("This is a string");
    }

    @Test
    public void cannotConvertIfNotAssignableFromString() {

        assertThatThrownBy(() -> converter.convert(BinaryFieldData.builder(0, (short) 15).build(), 1024))
                .isInstanceOf(DessicationFailure.class)
                .hasMessage("'CONVERSION_ERROR' - Could not convert value '1024' to bytes using String conversion - type 'class java.lang.Integer' not assignable from String.class.");
    }
}