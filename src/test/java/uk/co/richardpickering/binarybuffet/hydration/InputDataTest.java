package uk.co.richardpickering.binarybuffet.hydration;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static uk.co.richardpickering.binarybuffet.ByteOrder.BIG_ENDIAN;

public class InputDataTest {

    @Test
    public void deserialisesIntegerValues() {

        byte[] arr = { (byte) 40, (byte) 10, (byte) 10, (byte) 10 };
        ByteBuffer buf = ByteBuffer.wrap(arr);

        InputData input = new InputData(BIG_ENDIAN, ImmutableList.of((short) 40, (short) 10, (short) 10, (short) 10));

        assertThat(input.getInteger(0, (short) 4)).isEqualTo(buf.getInt());
    }
}