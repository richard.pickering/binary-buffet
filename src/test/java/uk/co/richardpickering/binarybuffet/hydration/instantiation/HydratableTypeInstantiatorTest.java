package uk.co.richardpickering.binarybuffet.hydration.instantiation;

import org.junit.Test;
import uk.co.richardpickering.binarybuffet.hydration.Hydratable;
import uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;

public class HydratableTypeInstantiatorTest {

    private final HydratableTypeInstantiator typeInstantiator = new HydratableTypeInstantiator();

    @Test
    public void canInstantiateValidClass() {

        ValidClass instance = typeInstantiator.createInstance(ValidClass.class);

        assertThat(instance).isNotNull();
    }

    @Test
    public void cannotInstantiatePrivateClass() {

        assertThatThrownBy(() -> typeInstantiator.createInstance(PrivateClass.class))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': Could not hydrate as the class 'class uk.co.richardpickering.binarybuffet.hydration.instantiation.HydratableTypeInstantiatorTest$PrivateClass' is not public.");
    }

    @Test
    public void cannotInstantiateClassWithoutDefaultConstructor() {

        assertThatThrownBy(() -> typeInstantiator.createInstance(DefaultConstructorlessClass.class))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': Could not hydrate as there is no default (parameterless) constructor for type 'class uk.co.richardpickering.binarybuffet.hydration.instantiation.HydratableTypeInstantiatorTest$DefaultConstructorlessClass'");
    }

    @Test
    public void cannotInstantiateClassIfAbstract() {

        assertThatThrownBy(() -> typeInstantiator.createInstance(AbstractClass.class))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': Could not hydrate as type 'class uk.co.richardpickering.binarybuffet.hydration.instantiation.HydratableTypeInstantiatorTest$AbstractClass' is abstract.");
    }

    @Test
    public void cannotInstantiateIfExceptionThrownOnConstructor() {

        assertThatThrownBy(() -> typeInstantiator.createInstance(FailingConstructorClass.class))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': Could not hydrate as exception thrown on default (parameterless) constructor invocation for class 'class uk.co.richardpickering.binarybuffet.hydration.instantiation.HydratableTypeInstantiatorTest$FailingConstructorClass'");
    }

    public static class ValidClass implements Hydratable {

        @Override
        public int getWidth() {
            return 0;
        }
    }

    private static class PrivateClass implements Hydratable {

        @Override
        public int getWidth() {
            return 0;
        }
    }

    public static class DefaultConstructorlessClass implements Hydratable {

        public DefaultConstructorlessClass(final int i) {

        }


        @Override
        public int getWidth() {
            return 0;
        }
    }

    public static abstract class AbstractClass implements Hydratable {

        @Override
        public int getWidth() {
            return 0;
        }
    }

    public static class PrivateConstructorClass implements Hydratable {

        private PrivateConstructorClass() {

        }

        @Override
        public int getWidth() {
            return 0;
        }
    }

    public static class FailingConstructorClass implements Hydratable {

        public FailingConstructorClass() {
            throw new IllegalStateException("Something went wrong!");
        }

        @Override
        public int getWidth() {
            return 0;
        }
    }
}