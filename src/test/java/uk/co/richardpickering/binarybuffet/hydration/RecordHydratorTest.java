package uk.co.richardpickering.binarybuffet.hydration;

import org.junit.Test;
import uk.co.richardpickering.binarybuffet.annotation.BinaryField;
import uk.co.richardpickering.binarybuffet.hydration.BinaryDataInput.InputStreamBinaryInput;
import uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;

public class RecordHydratorTest {

    private static final Long INT_VALUE = 128491L;
    private static final String STRING_VALUE = "hello!";

    private static final byte[] RECORD_BYTES = {
            12, /* <-- Record 1 - Record indicator */
            0, 1, (byte) 0b11110101, (byte) 0b11101011, /* <-- Integer value */
            104, 101, 108, 108, 111, 33, /* <-- String value */

            5, /* <-- Record 2 - Record indicator */
            0, 1, (byte) 0b11110101, (byte) 0b11101011, /* <-- Integer value */
            104, 101, 108, 108, 111, 33, /* <-- String value */
            0, 1, (byte) 0b11110101, (byte) 0b11101011, /* <-- Integer value */

            12, /* <-- Record 1 - Record indicator */
            0, 1, (byte) 0b11110101, (byte) 0b11101011, /* <-- Integer value */
            104, 101, 108, 108, 111, 33, /* <-- String value */
    };

    @Test
    public void hydratesRecord() {

        InputStream inputStream = new ByteArrayInputStream(RECORD_BYTES);

        RecordHydrator hydrator = RecordHydrator.builder()
                .forIdentifier(0, (short) 12, ExampleRecord.class)
                .forIdentifier(0, (short) 5, ExampleRecord2.class)
                .build();

        HydrationResult result = hydrator.hydrate(new InputStreamBinaryInput(inputStream));

        ExampleRecord record = result.getRecordsOfType(ExampleRecord.class).get(0);

        assertThat(record.getIntField()).isEqualTo(INT_VALUE);
        assertThat(record.getStringField()).isEqualTo(STRING_VALUE);

        record = result.getRecordsOfType(ExampleRecord.class).get(1);

        assertThat(record.getIntField()).isEqualTo(INT_VALUE);
        assertThat(record.getStringField()).isEqualTo(STRING_VALUE);

        ExampleRecord2 record2 = result.getRecordsOfType(ExampleRecord2.class).get(0);

        assertThat(record2.getIntField()).isEqualTo(INT_VALUE);
        assertThat(record2.getSecondIntField()).isEqualTo(INT_VALUE);
        assertThat(record2.getStringField()).isEqualTo(STRING_VALUE);
    }

    @Test
    public void failsIfNotEnoughInputForEvenSmallestRecord() {

        byte[] bytes = { 12, 0, 1 };

        InputStream inputStream = new ByteArrayInputStream(bytes);

        RecordHydrator hydrator = RecordHydrator.builder()
                .forIdentifier(0, (short) 12, ExampleRecord.class)
                .forIdentifier(0, (short) 5, ExampleRecord2.class)
                .build();

        assertThatThrownBy(() -> hydrator.hydrate(new InputStreamBinaryInput(inputStream)))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'NOT_ENOUGH_INPUT': Could not hydrate records as only '3' bytes were contained in the input which is less than the minimum number of bytes required for the smallest registered record ('11').");
    }

    @Test
    public void byDefaultFailsIfUnrecognisedInput() {

        byte[] bytes = { 50, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 };

        InputStream inputStream = new ByteArrayInputStream(bytes);

        RecordHydrator hydrator = RecordHydrator.builder()
                .forIdentifier(0, (short) 12, ExampleRecord.class)
                .forIdentifier(0, (short) 5, ExampleRecord2.class)
                .build();

        assertThatThrownBy(() -> hydrator.hydrate(new InputStreamBinaryInput(inputStream)))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'UNRECOGNISED_INPUT': Found unrecognised input data 'InputData{values=[50, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]}'");
    }

    public static class ExampleRecord implements Hydratable {

        @BinaryField(offset = 0, width = 1)
        private Short identifier;

        @BinaryField(offset = 1, width = 4)
        private Long intField;

        @BinaryField(offset = 5, width = 6)
        private String stringField;

        public ExampleRecord() {

        }

        Long getIntField() {
            return intField;
        }

        String getStringField() {
            return stringField;
        }

        @Override
        public int getWidth() {
            return 11;
        }
    }

    public static class ExampleRecord2 implements Hydratable {

        @BinaryField(offset = 0, width = 1)
        private Short identifier;

        @BinaryField(offset = 1, width = 4)
        private Long intField;

        @BinaryField(offset = 5, width = 6)
        private String stringField;

        @BinaryField(offset = 11, width = 4)
        private Long secondIntField;

        public ExampleRecord2() {

        }

        Long getIntField() {
            return intField;
        }

        String getStringField() {
            return stringField;
        }

        Long getSecondIntField() {
            return secondIntField;
        }

        @Override
        public int getWidth() {
            return 15;
        }
    }
}
