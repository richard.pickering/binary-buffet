package uk.co.richardpickering.binarybuffet.hydration.instantiation;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import uk.co.richardpickering.binarybuffet.FieldHydrationConversion;
import uk.co.richardpickering.binarybuffet.annotation.BinaryField;
import uk.co.richardpickering.binarybuffet.hydration.Hydratable;
import uk.co.richardpickering.binarybuffet.hydration.InputData;
import uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static uk.co.richardpickering.binarybuffet.ByteOrder.BIG_ENDIAN;

public class BinaryFieldDeserialiserTest {

    private static final Long INT_VALUE = 128491L;
    private static final String STRING_VALUE = "hello!";

    private static final List<Short> RECORD = ImmutableList.<Short>builder()
            .add((short) 12) /* <-- Record 1 - Record indicator */
            .add((short) 0, (short) 1, (short) 0b11110101, (short) 0b11101011) /* <-- Integer value */
            .add((short) 104, (short) 101, (short) 108, (short) 108, (short) 111, (short) 33) /* <-- String value */
            .build();

    private final BinaryFieldDeserialiser deserialiser = new BinaryFieldDeserialiser();

    @Test
    public void deserialises() {

        Class<DeserialisableRecord> clazz = DeserialisableRecord.class;
        DeserialisableRecord instance = new DeserialisableRecord();

        assertThat(instance.getLongField()).isNull();
        assertThat(instance.getStringField()).isNull();

        deserialiser.deserialiseFields(clazz, new InputData(BIG_ENDIAN, RECORD), instance);

        assertThat(instance.getLongField()).isEqualTo(INT_VALUE);
        assertThat(instance.getStringField()).isEqualTo(STRING_VALUE);

        assertThat(instance.getIntField()).isEqualTo(128491);
        assertThat(instance.getByteField()).isEqualTo((byte) -11);
        assertThat(instance.getShortField()).isEqualTo((short) -2581);
    }

    @Test
    public void cannotDeserialiseIfTypeNotRegistered() {

        Class<ClassWithUnsupportedType> clazz = ClassWithUnsupportedType.class;
        ClassWithUnsupportedType instance = new ClassWithUnsupportedType();

        assertThatThrownBy(() -> deserialiser.deserialiseFields(clazz, new InputData(BIG_ENDIAN, RECORD), instance))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': No field level hydration conversion or hydration field conversion registered for type 'class java.lang.Boolean'");
    }

    @Test
    public void usesCustomHydratorIfAvailable() {

        Class<ClassWithCustomHydrator> clazz = ClassWithCustomHydrator.class;
        ClassWithCustomHydrator instance = new ClassWithCustomHydrator();

        assertThat(instance.getValue()).isNull();

        deserialiser.deserialiseFields(clazz, new InputData(BIG_ENDIAN, RECORD), instance);

        assertThat(instance.getValue()).isEqualTo(1024);
    }

    @Test
    public void cannotConvertIfConverterDoesNotProviderAssignableType() {

        Class<NotAssignableHydrator> clazz = NotAssignableHydrator.class;
        NotAssignableHydrator instance = new NotAssignableHydrator();

        assertThat(instance.getValue()).isNull();

        assertThatThrownBy(() -> deserialiser.deserialiseFields(clazz, new InputData(BIG_ENDIAN, RECORD), instance))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': Cannot set field 'private java.lang.String "
                        + "uk.co.richardpickering.binarybuffet.hydration.instantiation.BinaryFieldDeserialiserTest$NotAssignableHydrator.value' "
                        + "with value '1024' - type mismatch!");
    }

    public static class DeserialisableRecord implements Hydratable {

        @BinaryField(offset = 1, width = 4)
        private Long longField;

        @BinaryField(offset = 5, width = 6)
        private String stringField;

        @BinaryField(offset = 1, width = 4)
        private Integer intField;

        @BinaryField(offset = 3, width = 1)
        private Byte byteField;

        @BinaryField(offset = 3, width = 2)
        private Short shortField;

        DeserialisableRecord() {

        }

        Long getLongField() {
            return longField;
        }

        String getStringField() {
            return stringField;
        }

        Integer getIntField() {
            return intField;
        }

        Byte getByteField() {
            return byteField;
        }

        short getShortField() {
            return shortField;
        }

        @Override
        public int getWidth() {
            return 10;
        }
    }

    public static class ClassWithUnsupportedType implements Hydratable {

        @BinaryField(width = 1, offset = 0)
        private Boolean value;

        @Override
        public int getWidth() {
            return 1;
        }
    }

    public static class ClassWithCustomHydrator implements Hydratable {

        @BinaryField(offset = 1, width = 4, hydration = CustomHydrator.class)
        private Integer value;

        @Override
        public int getWidth() {
            return 10;
        }

        public Integer getValue() {
            return value;
        }
    }

    public static class NotAssignableHydrator implements Hydratable {

        @BinaryField(offset = 1, width = 4, hydration = CustomHydrator.class)
        private String value;

        @Override
        public int getWidth() {
            return 10;
        }

        public String getValue() {
            return value;
        }
    }

    public static class CustomHydrator implements FieldHydrationConversion<Integer> {

        @Override
        public Integer convert(final InputData inputData, final BinaryFieldData binaryFieldData) {
            return 1024;
        }
    }
}