package uk.co.richardpickering.binarybuffet.hydration.extraction;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import uk.co.richardpickering.binarybuffet.hydration.InputData;

import java.nio.ByteBuffer;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static uk.co.richardpickering.binarybuffet.ByteOrder.BIG_ENDIAN;
import static uk.co.richardpickering.binarybuffet.ByteOrder.LITTLE_ENDIAN;

public class ByteConcatenatorTest {

    @Test
    public void concatsIntBigEndian() {

        byte[] arr = { (byte) 40, (byte) 10, (byte) 10, (byte) 10 };
        ByteBuffer buf = ByteBuffer.wrap(arr);

        InputData input = new InputData(BIG_ENDIAN, ImmutableList.of((short) 40, (short) 10, (short) 10, (short) 10));

        assertThat(ByteConcatenator.concatBytesToInt(BIG_ENDIAN, 0, (short) 4, input))
                .isEqualTo(buf.getInt());
    }

    @Test
    public void concatsIntLittleEndian() {

        byte[] arr = { (byte) 40, (byte) 10, (byte) 10, (byte) 10 };
        ByteBuffer buf = ByteBuffer.wrap(arr);

        InputData input = new InputData(BIG_ENDIAN, ImmutableList.of((short) 10, (short) 10, (short) 10, (short) 40));

        assertThat(ByteConcatenator.concatBytesToInt(LITTLE_ENDIAN, 0, (short) 4, input))
                .isEqualTo(buf.getInt());
    }
}