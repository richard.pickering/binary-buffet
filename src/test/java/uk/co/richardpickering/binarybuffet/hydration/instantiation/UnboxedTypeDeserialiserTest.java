package uk.co.richardpickering.binarybuffet.hydration.instantiation;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UnboxedTypeDeserialiserTest {

    private static final UnboxedTypeDeserialiser DESERIALISER = new UnboxedTypeDeserialiser();

    @Test
    public void canConvertByte() throws Exception {
        assertThat(DESERIALISER.getDeserialiser(Example.class.getField("byteProperty"))).isPresent();
    }

    @Test
    public void canConvertShort() throws Exception {
        assertThat(DESERIALISER.getDeserialiser(Example.class.getField("shortProperty"))).isPresent();
    }

    @Test
    public void canConvertInt() throws Exception {
        assertThat(DESERIALISER.getDeserialiser(Example.class.getField("intProperty"))).isPresent();
    }

    @Test
    public void canConvertLong() throws Exception {
        assertThat(DESERIALISER.getDeserialiser(Example.class.getField("shortProperty"))).isPresent();
    }

    private static class Example {
        public byte byteProperty;
        public short shortProperty;
        public int intProperty;
        public long longProperty;
    }
}