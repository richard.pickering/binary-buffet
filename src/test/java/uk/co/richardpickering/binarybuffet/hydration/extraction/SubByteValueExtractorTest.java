package uk.co.richardpickering.binarybuffet.hydration.extraction;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;

public class SubByteValueExtractorTest {

    private final SubByteValueExtractor extractor = new SubByteValueExtractor();

    @Test
    public void extractsSubByteValue() {
        assertThat(extractor.extract((byte) 2, (byte) 5, (short) 0b10110011))
                .isEqualTo((short) 0b1100);
    }

    @Test
    public void fromBitMustBeValid() {

        assertThatThrownBy(() -> extractor.extract((byte) -1, (byte) 5, (short) 10))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("fromBit must be >= 0! Actual: '-1'");

        assertThatThrownBy(() -> extractor.extract((byte) 8, (byte) 5, (short) 10))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("fromBit must be <= 7! Actual: '8'");
    }

    @Test
    public void toBitMustBeValid() {

        assertThatThrownBy(() -> extractor.extract((byte) 5, (byte) 2, (short) 10))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("toBit must be >= fromBit! Actual toBit: '5', fromBit: '2'");

        assertThatThrownBy(() -> extractor.extract((byte) 4, (byte) 8, (short) 10))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("toBit must be <= 7! Actual: '8'");
    }
}