package uk.co.richardpickering.binarybuffet.hydration.instantiation;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import uk.co.richardpickering.binarybuffet.annotation.BinaryField;
import uk.co.richardpickering.binarybuffet.hydration.Hydratable;
import uk.co.richardpickering.binarybuffet.hydration.InputData;
import uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import static uk.co.richardpickering.binarybuffet.ByteOrder.BIG_ENDIAN;

public class HydratableTypeDeserialiserTest {

    private static final Long INT_VALUE = 128491L;
    private static final String STRING_VALUE = "hello!";

    private static final List<Short> RECORD = ImmutableList.<Short>builder()
            .add((short) 12) /* <-- Record 1 - Record indicator */
            .add((short) 0, (short) 1, (short) 0b11110101, (short) 0b11101011) /* <-- Integer value */
            .add((short) 104, (short) 101, (short) 108, (short) 108, (short) 111, (short) 33) /* <-- String value */
            .build();

    private HydratableTypeDeserialiser typeDeserialiser = new HydratableTypeDeserialiser(new BinaryFieldDeserialiser());

    @Test
    public void deserialises() {
        ExampleRecord deserialised = typeDeserialiser.deserialiseHydratable(ExampleRecord.class, new InputData(BIG_ENDIAN, RECORD));

        assertThat(deserialised.getIntField()).isEqualTo(INT_VALUE);
        assertThat(deserialised.getStringField()).isEqualTo(STRING_VALUE);
    }

    @Test
    public void cannotDeserialisePrivateClass() {

        assertThatThrownBy(() -> typeDeserialiser.deserialiseHydratable(PrivateClass.class, new InputData(BIG_ENDIAN, RECORD)))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': Could not hydrate as the class "
                        + "'class uk.co.richardpickering.binarybuffet.hydration.instantiation.HydratableTypeDeserialiserTest$PrivateClass' "
                        + "is not public.");
    }

    @Test
    public void cannotDeserialiseClassWithoutDefaultConstructor() {

        assertThatThrownBy(() -> typeDeserialiser.deserialiseHydratable(DefaultConstructorlessClass.class, new InputData(BIG_ENDIAN, RECORD)))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': Could not hydrate as there is no default (parameterless) constructor for type 'class uk.co.richardpickering.binarybuffet.hydration.instantiation.HydratableTypeDeserialiserTest$DefaultConstructorlessClass'");
    }

    @Test
    public void cannotDeserialiseClassIfAbstract() {

        assertThatThrownBy(() -> typeDeserialiser.deserialiseHydratable(AbstractClass.class, new InputData(BIG_ENDIAN, RECORD)))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': Could not hydrate as type 'class uk.co.richardpickering.binarybuffet.hydration.instantiation.HydratableTypeDeserialiserTest$AbstractClass' is abstract.");
    }

    @Test
    public void cannotDeserialiseIfExceptionThrownOnConstructor() {

        assertThatThrownBy(() -> typeDeserialiser.deserialiseHydratable(FailingConstructorClass.class, new InputData(BIG_ENDIAN, RECORD)))
                .isInstanceOf(HydrationFailure.class)
                .hasMessage("'HYDRATION_TARGET_INVALID': Could not hydrate as exception thrown on default (parameterless) constructor invocation for class 'class uk.co.richardpickering.binarybuffet.hydration.instantiation.HydratableTypeDeserialiserTest$FailingConstructorClass'");
    }

    public static class ExampleRecord implements Hydratable {

        @BinaryField(offset = 1, width = 4)
        public Long intField;

        @BinaryField(offset = 5, width = 6)
        public String stringField;

        public ExampleRecord() {

        }

        public Long getIntField() {
            return intField;
        }

        public String getStringField() {
            return stringField;
        }

        @Override
        public int getWidth() {
            return 10;
        }
    }

    private static class PrivateClass implements Hydratable {

        @Override
        public int getWidth() {
            return 0;
        }
    }

    public static class DefaultConstructorlessClass implements Hydratable {

        public DefaultConstructorlessClass(final int i) {

        }


        @Override
        public int getWidth() {
            return 0;
        }
    }

    public static abstract class AbstractClass implements Hydratable {

        @Override
        public int getWidth() {
            return 0;
        }
    }

    public static class PrivateConstructorClass implements Hydratable {

        private PrivateConstructorClass() {

        }

        @Override
        public int getWidth() {
            return 0;
        }
    }

    public static class FailingConstructorClass implements Hydratable {

        public FailingConstructorClass() {
            throw new IllegalStateException("Something went wrong!");
        }

        @Override
        public int getWidth() {
            return 0;
        }
    }
}