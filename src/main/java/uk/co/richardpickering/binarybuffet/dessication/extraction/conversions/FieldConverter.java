package uk.co.richardpickering.binarybuffet.dessication.extraction.conversions;

import uk.co.richardpickering.binarybuffet.ByteOrder;
import uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static uk.co.richardpickering.binarybuffet.ByteOrder.BIG_ENDIAN;
import static uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure.DessicationFailureType.CONVERSION_ERROR;

public class FieldConverter {

    private final Map<Class<?>, FieldConversion> registeredConversions = new HashMap<>();

    private FieldConverter(final Builder builder) {
        this.registeredConversions.putAll(builder.registeredConversions);

        registerDefaultsIfNotSet(builder.byteOrder);
    }

    public byte[] convertField(final BinaryFieldData binaryField, final Class<?> fieldType, final Object value) {

        return binaryField.getDessicationConversion()
                .map(conversion -> conversion.convert(value))
                .orElseGet(() -> useTypeInferredConversion(binaryField, fieldType, value));
    }

    private byte[] useTypeInferredConversion(final BinaryFieldData binaryField, final Class<?> fieldType, final Object value) {
        return getConversion(fieldType)
                .map(conversion -> conversion.convert(binaryField, value))
                .orElseThrow(() -> DessicationFailure.builder(CONVERSION_ERROR, "Could not convert field - "
                        + "no registered conversion for type '%s'", fieldType)
                        .build());
    }

    private void registerDefaultsIfNotSet(final ByteOrder byteOrder) {

        IntegerByteExtractor extractor = new IntegerByteExtractor(byteOrder);

        byteConversions(extractor);
        shortConversions(extractor);
        intConversions(extractor);
        longConversions(extractor);

        if (!hasConversion(String.class)) {
            registeredConversions.put(String.class, new StringConverter());
        }
    }

    private void byteConversions(final IntegerByteExtractor extractor) {

        if (!hasConversion(Byte.class)) {
            registeredConversions.put(Byte.class, new IntegerConverter(extractor));
        }

        if (!hasConversion(byte.class)) {
            registeredConversions.put(byte.class, new IntegerConverter(extractor));
        }
    }

    private void shortConversions(final IntegerByteExtractor extractor) {

        if (!hasConversion(Short.class)) {
            registeredConversions.put(Short.class, new IntegerConverter(extractor));
        }

        if (!hasConversion(short.class)) {
            registeredConversions.put(short.class, new IntegerConverter(extractor));
        }
    }

    private void intConversions(final IntegerByteExtractor extractor) {

        if (!hasConversion(Integer.class)) {
            registeredConversions.put(Integer.class, new IntegerConverter(extractor));
        }

        if (!hasConversion(int.class)) {
            registeredConversions.put(int.class, new IntegerConverter(extractor));
        }
    }

    private void longConversions(final IntegerByteExtractor extractor) {

        if (!hasConversion(Long.class)) {
            registeredConversions.put(Long.class, new IntegerConverter(extractor));
        }

        if (!hasConversion(long.class)) {
            registeredConversions.put(long.class, new IntegerConverter(extractor));
        }
    }

    private Optional<FieldConversion> getConversion(final Class<?> type) {
        return Optional.ofNullable(registeredConversions.get(type));
    }

    private boolean hasConversion(final Class<?> type) {
        return registeredConversions.containsKey(type);
    }

    public interface FieldConversion {
        byte[] convert(BinaryFieldData field, Object value);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private ByteOrder byteOrder = BIG_ENDIAN;
        private final Map<Class<?>, FieldConversion> registeredConversions = new HashMap<>();

        private Builder() {

        }

        public Builder withConversion(final Class<?> type, final FieldConversion conversion) {
            registeredConversions.put(type, conversion);
            return this;
        }

        public Builder withByteOrder(final ByteOrder order) {
            this.byteOrder = order;
            return this;
        }

        public FieldConverter build() {
            return new FieldConverter(this);
        }
    }
}
