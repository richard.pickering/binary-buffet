package uk.co.richardpickering.binarybuffet.dessication.extraction.conversions;

import org.apache.commons.lang3.ClassUtils;
import uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure;
import uk.co.richardpickering.binarybuffet.dessication.extraction.conversions.FieldConverter.FieldConversion;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;

import static uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure.DessicationFailureType.CONVERSION_ERROR;

public class IntegerConverter implements FieldConversion {

    private final IntegerByteExtractor integerByteExtractor;

    public IntegerConverter(final IntegerByteExtractor integerByteExtractor) {
        this.integerByteExtractor = integerByteExtractor;
    }

    @Override
    public byte[] convert(final BinaryFieldData binaryField, final Object value) {

        long val = convertToLong(value);

        return integerByteExtractor.extract(val, binaryField.getWidth());
    }

    private long convertToLong(final Object value) {

        Class<?> clazz = value.getClass();

        if (ClassUtils.isAssignable(Long.class, clazz, true)) {
            return (long) value;
        }

        if (ClassUtils.isAssignable(Integer.class, clazz, true)) {
            return (long) (int) value;
        }

        if (ClassUtils.isAssignable(Short.class, clazz, true)) {
            return (long) (short) value;
        }

        if (ClassUtils.isAssignable(Byte.class, clazz, true)) {
            return (long) (byte) value;
        }

        throw DessicationFailure.builder(CONVERSION_ERROR, "Could not convert value '%s' to bytes using integer "
                + "conversion - type '%s' not assignable from Byte.class, Short.class, Integer.class or Long.class.", value, clazz)
                .build();
    }
}
