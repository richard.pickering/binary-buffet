package uk.co.richardpickering.binarybuffet.dessication;

public class DessicationRecord<T> {

    private final Class<T> type;
    private final T instance;

    public DessicationRecord(final Class<T> type, final T instance) {

        this.type = type;
        this.instance = instance;
    }

    public Class<T> getType() {
        return type;
    }

    public T getInstance() {
        return instance;
    }
}
