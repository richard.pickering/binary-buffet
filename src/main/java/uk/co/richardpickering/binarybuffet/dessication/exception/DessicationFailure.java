package uk.co.richardpickering.binarybuffet.dessication.exception;

import static java.lang.String.format;

public class DessicationFailure extends RuntimeException {

    public enum DessicationFailureType {
        IO_ERROR,
        CONVERSION_ERROR,
        DESSICATION_TARGET_INVALID
    }

    private final DessicationFailureType failureType;

    public DessicationFailure(final Builder builder) {
        super(format("'%s' - " + format(builder.message, builder.variables), builder.failureType), builder.cause);
        this.failureType = builder.failureType;
    }

    public DessicationFailureType getFailureType() {
        return failureType;
    }

    public static Builder builder(final DessicationFailureType failureType,
                                  final String message,
                                  final Object... variables) {

        return new Builder(failureType, message, variables);
    }

    public static class Builder {

        private final DessicationFailureType failureType;
        private final String message;
        private final Object[] variables;
        private Exception cause = null;

        private Builder(final DessicationFailureType failureType, final String message, final Object... variables) {

            this.failureType = failureType;
            this.message = message;
            this.variables = variables;
        }

        public Builder withCause(final Exception e) {
            this.cause = e;
            return this;
        }

        public DessicationFailure build() {
            return new DessicationFailure(this);
        }
    }
}
