package uk.co.richardpickering.binarybuffet.dessication;

/**
 * Dessicates (serialises) data records (implementing Dessicatable) into bytes and publishes the bytes to the
 * provided BinaryDataOutput.
 */
public class Dessicator {

    private final RecordDessicator recordDessicator;

    public Dessicator(final RecordDessicator recordDessicator) {
        this.recordDessicator = recordDessicator;
    }

    public void dessicate(final DessicationRecordPayload records, final BinaryDataOutput output) {

        records.forEach(record -> output.write(recordDessicator.dessicate(record)));
    }
}
