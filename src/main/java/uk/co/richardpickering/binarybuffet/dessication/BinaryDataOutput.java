package uk.co.richardpickering.binarybuffet.dessication;

import uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure;

import java.io.IOException;
import java.io.OutputStream;

import static uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure.DessicationFailureType.IO_ERROR;

public interface BinaryDataOutput {

    void write(byte[] data);
    void close();

    class OutputStreamBinaryDataOutput implements BinaryDataOutput {

        private final OutputStream outputStream;

        public OutputStreamBinaryDataOutput(final OutputStream outputStream) {
            this.outputStream = outputStream;
        }

        @Override
        public void write(byte[] data) {

            try {

                outputStream.write(data);

            } catch (final IOException e) {
                throw DessicationFailure.builder(IO_ERROR, "Could not write '%s' to output stream.", data)
                        .withCause(e)
                        .build();
            }
        }

        @Override
        public void close() {

            try {

                outputStream.close();

            } catch (final IOException e) {
                throw DessicationFailure.builder(IO_ERROR, "Could not close output stream.")
                        .withCause(e)
                        .build();
            }
        }
    }
}
