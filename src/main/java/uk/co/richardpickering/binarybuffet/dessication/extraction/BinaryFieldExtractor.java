package uk.co.richardpickering.binarybuffet.dessication.extraction;

import uk.co.richardpickering.binarybuffet.dessication.DessicationRecord;
import uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure;
import uk.co.richardpickering.binarybuffet.dessication.extraction.conversions.FieldConverter;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldDataExtractor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static com.google.common.base.Preconditions.checkState;
import static uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure.DessicationFailureType.DESSICATION_TARGET_INVALID;

public class BinaryFieldExtractor {

    private final FieldConverter fieldConverter;
    private final BinaryFieldDataExtractor binaryFieldDataExtractor;

    public BinaryFieldExtractor(final FieldConverter fieldConverter) {
        this.fieldConverter = fieldConverter;
        this.binaryFieldDataExtractor = new BinaryFieldDataExtractor();
    }

    public List<BinaryFieldValue> extractBinaryFields(final DessicationRecord<?> record) {

        Map<Field, BinaryFieldData> fields = binaryFieldDataExtractor.extract(record.getType());

        List<BinaryFieldValue> values = new ArrayList<>();

        for (final Entry<Field, BinaryFieldData> entry : fields.entrySet()) {

            Field field = entry.getKey();
            BinaryFieldData binaryField = entry.getValue();

            field.setAccessible(true);

            Object value = getFieldValue(field, record.getInstance());

            byte[] data = fieldConverter.convertField(binaryField, field.getType(), value);

            values.add(new BinaryFieldValue(data, binaryField.getOffset(), binaryField.getWidth()));
        }

        return values;
    }

    private Object getFieldValue(final Field field, final Object instance) {

        try {
            return field.get(instance);
        } catch (final IllegalAccessException e) {
            throw DessicationFailure.builder(DESSICATION_TARGET_INVALID, "Could not get field value for field '%s' "
                    + "for instance of type '%s'", field, instance.getClass())
                    .withCause(e)
                    .build();
        }
    }

    public static class BinaryFieldValue {

        private final byte[] value;
        private final int offset;
        private final int width;

        public BinaryFieldValue(final byte[] value, final int offset, final int width) {
            this.value = value;
            this.offset = offset;
            this.width = width;
        }

        public byte getByte(final int dataOffset) {

            checkState(dataOffset < value.length, "Cannot select byte from beyond value's byte length. Attempted to get "
                    + "byte '%s' out of '%s'", dataOffset, value.length);

            return value[dataOffset];
        }

        public int getBinaryFieldWidth() {
            return width;
        }

        public int getProvidedDataWidth() {
            return value.length;
        }

        public int getOffset() {
            return offset;
        }
    }
}
