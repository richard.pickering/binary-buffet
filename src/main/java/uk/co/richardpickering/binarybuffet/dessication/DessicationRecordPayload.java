package uk.co.richardpickering.binarybuffet.dessication;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * An object which contains a number of dessication records which are to be dessicated (serialised) in the order in which
 * they were provided.
 */
public class DessicationRecordPayload {

    private final List<DessicationRecord<?>> dessicationRecords = new ArrayList<>();

    private DessicationRecordPayload(final Builder builder) {
        this.dessicationRecords.addAll(builder.dessicationRecords);
    }

    void forEach(final Consumer<DessicationRecord<?>> consumer) {
        dessicationRecords.forEach(consumer);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final List<DessicationRecord<?>> dessicationRecords = new ArrayList<>();

        private Builder() {

        }

        public <T> Builder withRecord(final T instance, final Class<T> type) {
            dessicationRecords.add(new DessicationRecord<>(type, instance));
            return this;
        }

        public DessicationRecordPayload build() {
            return new DessicationRecordPayload(this);
        }
    }
}
