package uk.co.richardpickering.binarybuffet.dessication;

import uk.co.richardpickering.binarybuffet.dessication.extraction.BinaryFieldExtractor;
import uk.co.richardpickering.binarybuffet.dessication.extraction.BinaryFieldExtractor.BinaryFieldValue;

import java.util.List;

/**
 * Dessicates a DessicationRecord into bytes and returns those bytes.
 */
public class RecordDessicator {

    private final BinaryFieldExtractor binaryFieldExtractor;

    public RecordDessicator(final BinaryFieldExtractor binaryFieldExtractor) {

        this.binaryFieldExtractor = binaryFieldExtractor;
    }

    byte[] dessicate(final DessicationRecord<?> record) {

        List<BinaryFieldValue> fieldValues = binaryFieldExtractor.extractBinaryFields(record);

        int totalRecordSize = fieldValues.stream()
                .mapToInt(BinaryFieldValue::getBinaryFieldWidth)
                .sum();

        byte[] buffer = new byte[totalRecordSize];

        for (BinaryFieldValue fieldValue : fieldValues) {

            int width = fieldValue.getProvidedDataWidth();

            for (int i = 0; i < width; i++) {

                buffer[fieldValue.getOffset() + i] = fieldValue.getByte(i);
            }
        }

        return buffer;
    }
}
