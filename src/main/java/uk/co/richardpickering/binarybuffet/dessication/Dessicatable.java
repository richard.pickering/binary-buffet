package uk.co.richardpickering.binarybuffet.dessication;

/**
 * Represents an object to be 'dessicated' (serialised) into bytes.
 */
public interface Dessicatable {

    int getWidth();
}
