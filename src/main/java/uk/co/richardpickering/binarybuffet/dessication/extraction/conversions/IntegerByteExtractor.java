package uk.co.richardpickering.binarybuffet.dessication.extraction.conversions;

import uk.co.richardpickering.binarybuffet.ByteOrder;

import static uk.co.richardpickering.binarybuffet.ByteOrder.BIG_ENDIAN;

public class IntegerByteExtractor {

    private static final int BITS_IN_A_BYTE = 8;
    private static final long BITMASK = 0xFF;

    private final ByteOrder byteOrder;

    public IntegerByteExtractor(final ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    byte[] extract(final long input, final short width) {

        byte[] results = new byte[width];

        for (int i = 0; i < width; i++) {

            if (byteOrder == BIG_ENDIAN) {

                int relativeIndex = Math.abs(i - width) - 1;
                results[i] = getByteAtOffset(input, (short) relativeIndex);

            } else {

                results[i] = getByteAtOffset(input, (short) i);
            }
        }

        return results;
    }

    private byte getByteAtOffset(final long value, final short offset) {

        return (byte) ((value >> (BITS_IN_A_BYTE * offset)) & BITMASK);
    }
}
