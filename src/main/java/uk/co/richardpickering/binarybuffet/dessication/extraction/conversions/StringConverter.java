package uk.co.richardpickering.binarybuffet.dessication.extraction.conversions;

import uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure;
import uk.co.richardpickering.binarybuffet.dessication.extraction.conversions.FieldConverter.FieldConversion;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;

import static java.nio.charset.StandardCharsets.UTF_8;
import static uk.co.richardpickering.binarybuffet.dessication.exception.DessicationFailure.DessicationFailureType.CONVERSION_ERROR;

public class StringConverter implements FieldConversion {

    @Override
    public byte[] convert(final BinaryFieldData field, final Object value) {

        if (!value.getClass().isAssignableFrom(String.class)) {

            throw DessicationFailure.builder(CONVERSION_ERROR, "Could not convert value '%s' to bytes using String "
                    + "conversion - type '%s' not assignable from String.class.", value, value.getClass())
                    .build();
        }

        return ((String) value).getBytes(UTF_8);
    }
}
