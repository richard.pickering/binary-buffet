package uk.co.richardpickering.binarybuffet.annotation;

import uk.co.richardpickering.binarybuffet.FieldDessicationConversion;
import uk.co.richardpickering.binarybuffet.FieldDessicationConversion.NoDessicationConversion;
import uk.co.richardpickering.binarybuffet.FieldHydrationConversion;
import uk.co.richardpickering.binarybuffet.FieldHydrationConversion.NoHydrationConversion;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target(FIELD)
public @interface BinaryField {

    /**
     * @return The byte the field starts at
     */
    int offset();

    /**
     * @return The width of the field in bytes
     */
    short width();

    Class<? extends FieldDessicationConversion> dessication() default NoDessicationConversion.class;
    Class<? extends FieldHydrationConversion<?>> hydration() default NoHydrationConversion.class;
}
