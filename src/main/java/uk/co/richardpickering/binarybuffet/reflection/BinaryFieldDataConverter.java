package uk.co.richardpickering.binarybuffet.reflection;

import uk.co.richardpickering.binarybuffet.FieldDessicationConversion;
import uk.co.richardpickering.binarybuffet.FieldDessicationConversion.NoDessicationConversion;
import uk.co.richardpickering.binarybuffet.FieldHydrationConversion;
import uk.co.richardpickering.binarybuffet.FieldHydrationConversion.NoHydrationConversion;
import uk.co.richardpickering.binarybuffet.annotation.BinaryField;
import uk.co.richardpickering.binarybuffet.reflection.exception.BinaryBuffetConfigException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

import static uk.co.richardpickering.binarybuffet.reflection.exception.BinaryBuffetConfigException.BinaryBuffetConfigExceptionType.INVALID_CUSTOM_DESSICATION;

class BinaryFieldDataConverter {

    static BinaryFieldData convert(final BinaryField binaryField) {

        Optional<FieldDessicationConversion> dessicationConversion = instantiate(binaryField.dessication(), NoDessicationConversion.class);
        Optional<FieldHydrationConversion> hydrationConversion = instantiate(binaryField.hydration(), NoHydrationConversion.class);

        BinaryFieldData.Builder builder = BinaryFieldData.builder(binaryField.offset(), binaryField.width());

        dessicationConversion.ifPresent(builder::withFieldDessicationConversion);
        hydrationConversion.ifPresent(builder::withFieldHydrationConversion);

        return builder.build();
    }

    private static <T> Optional<T> instantiate(final Class<? extends T> type, final Class<? extends T> disabledType) {

        if (type.isAssignableFrom(disabledType)) {
            return Optional.empty();
        }

        try {

            Constructor<? extends T> constructor = type.getDeclaredConstructor();

            constructor.setAccessible(true);

            return Optional.of(constructor.newInstance());
        } catch (final InstantiationException e) {
            throw new BinaryBuffetConfigException(INVALID_CUSTOM_DESSICATION, e, "Could not instantiate custom "
                    + "conversion of type '%s' as it is abstract", type);
        } catch (final IllegalAccessException e) {
            throw new BinaryBuffetConfigException(INVALID_CUSTOM_DESSICATION, e, "Could not instantiate custom "
                    + "conversion of type '%s' as default declared constructor is inaccessible", type);
        } catch (final InvocationTargetException e) {
            throw new BinaryBuffetConfigException(INVALID_CUSTOM_DESSICATION, e, "Could not instantiate custom "
                    + "conversion of type '%s' as the default declared constructor threw an exception", type);
        } catch (final NoSuchMethodException e) {
            throw new BinaryBuffetConfigException(INVALID_CUSTOM_DESSICATION, e, "Could not instantiate custom "
                    + "conversion as type '%s' has no default declared constructor", type);
        }
    }
}
