package uk.co.richardpickering.binarybuffet.reflection;

import uk.co.richardpickering.binarybuffet.FieldDessicationConversion;
import uk.co.richardpickering.binarybuffet.FieldHydrationConversion;

import java.util.Optional;

public class BinaryFieldData {

    private final int offset;
    private final short width;
    private final FieldDessicationConversion dessicationConversion;
    private final FieldHydrationConversion<?> hydrationConversion;

    private BinaryFieldData(final Builder builder) {
        this.offset = builder.offset;
        this.width = builder.width;
        this.dessicationConversion = builder.fieldDessicationConversion;
        this.hydrationConversion = builder.fieldHydrationConversion;
    }

    public int getOffset() {
        return offset;
    }

    public short getWidth() {
        return width;
    }

    public Optional<FieldDessicationConversion> getDessicationConversion() {
        return Optional.ofNullable(dessicationConversion);
    }

    public Optional<FieldHydrationConversion<?>> getHydrationConversion() {
        return Optional.ofNullable(hydrationConversion);
    }

    public static Builder builder(final int offset, final short width) {
        return new Builder(offset, width);
    }

    public static class Builder {

        private final int offset;
        private final short width;
        private FieldDessicationConversion fieldDessicationConversion;
        private FieldHydrationConversion<?> fieldHydrationConversion;

        private Builder(final int offset, final short width) {

            this.offset = offset;
            this.width = width;
        }

        public Builder withFieldDessicationConversion(final FieldDessicationConversion conversion) {
            this.fieldDessicationConversion = conversion;
            return this;
        }

        public Builder withFieldHydrationConversion(final FieldHydrationConversion<?> conversion) {
            this.fieldHydrationConversion = conversion;
            return this;
        }

        public BinaryFieldData build() {
            return new BinaryFieldData(this);
        }
    }
}
