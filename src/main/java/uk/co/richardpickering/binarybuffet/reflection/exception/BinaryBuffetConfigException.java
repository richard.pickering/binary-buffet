package uk.co.richardpickering.binarybuffet.reflection.exception;

import static java.lang.String.format;

public class BinaryBuffetConfigException extends RuntimeException {

    public enum BinaryBuffetConfigExceptionType {
        INVALID_CUSTOM_DESSICATION
    }

    private final BinaryBuffetConfigExceptionType failureType;

    public BinaryBuffetConfigException(final BinaryBuffetConfigExceptionType failureType,
                                       final Exception cause,
                                       final String message,
                                       final Object... variables) {

        super(format("'%s' - " + format(message, variables), failureType), cause);

        this.failureType = failureType;
    }
}
