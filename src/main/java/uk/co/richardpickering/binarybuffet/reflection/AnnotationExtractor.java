package uk.co.richardpickering.binarybuffet.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AnnotationExtractor {

    public <T extends Annotation> Optional<T> getClassLevelAnnotation(final Class<?> type,
                                                                      final Class<T> annotationType) {

        return Optional.ofNullable(type.getAnnotation(annotationType));
    }

    public <T extends Annotation> Map<Field, T> getFieldAnnotations(final Class<?> type,
                                                                    final Class<T> annotationClass) {

        Map<Field, T> fields = new HashMap<>();

        for (Field field : type.getDeclaredFields()) {

            T annotation = field.getAnnotation(annotationClass);

            if (annotation != null) {
                fields.put(field, annotation);
            }
        }

        return fields;
    }
}
