package uk.co.richardpickering.binarybuffet.reflection;

import uk.co.richardpickering.binarybuffet.annotation.BinaryField;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;

import static java.util.stream.Collectors.toMap;

public class BinaryFieldDataExtractor {

    private final AnnotationExtractor annotationExtractor = new AnnotationExtractor();

    public Map<Field, BinaryFieldData> extract(final Class<?> type) {

        return annotationExtractor.getFieldAnnotations(type, BinaryField.class).entrySet()
                .stream()
                .collect(toMap(Entry::getKey, entry -> BinaryFieldDataConverter.convert(entry.getValue())));
    }
}
