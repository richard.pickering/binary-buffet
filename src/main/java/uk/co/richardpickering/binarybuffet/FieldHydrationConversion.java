package uk.co.richardpickering.binarybuffet;

import uk.co.richardpickering.binarybuffet.hydration.InputData;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;

public interface FieldHydrationConversion<T> {

    T convert(InputData inputData, BinaryFieldData binaryFieldData);

    class NoHydrationConversion implements FieldHydrationConversion<Void> {

        @Override
        public Void convert(final InputData inputData, final BinaryFieldData binaryFieldData) {
            return null;
        }
    }
}
