package uk.co.richardpickering.binarybuffet;

@FunctionalInterface
public interface FieldDessicationConversion {

    byte[] convert(Object value);

    class NoDessicationConversion implements FieldDessicationConversion {

        @Override
        public byte[] convert(final Object value) {
            return new byte[0];
        }
    }
}
