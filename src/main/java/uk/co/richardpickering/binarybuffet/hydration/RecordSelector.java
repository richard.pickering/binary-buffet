package uk.co.richardpickering.binarybuffet.hydration;

@FunctionalInterface
public interface RecordSelector {

    boolean isRecord(final InputData possibleRecord);

    class ByByteIdentifier implements RecordSelector {
        private final int byteOffset;
        private final short identifier;

        public ByByteIdentifier(final int byteOffset, final short identifier) {
            this.byteOffset = byteOffset;
            this.identifier = identifier;
        }

        public int getByteOffset() {
            return byteOffset;
        }

        public short getIdentifier() {
            return identifier;
        }

        @Override
        public boolean isRecord(final InputData possibleRecord) {
            return possibleRecord.size() >= byteOffset
                    && possibleRecord.get(byteOffset) == identifier;
        }
    }
}
