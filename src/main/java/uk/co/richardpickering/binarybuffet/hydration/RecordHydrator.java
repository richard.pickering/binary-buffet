package uk.co.richardpickering.binarybuffet.hydration;

import uk.co.richardpickering.binarybuffet.ByteOrder;
import uk.co.richardpickering.binarybuffet.hydration.RecordSelector.ByByteIdentifier;
import uk.co.richardpickering.binarybuffet.hydration.UnrecognisedInputStrategy.FailOnUnrecognisedInput;
import uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;
import static uk.co.richardpickering.binarybuffet.ByteOrder.BIG_ENDIAN;
import static uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure.HydrationFailureType.NOT_ENOUGH_INPUT;

public class RecordHydrator {

    private final ByteOrder endianness;
    private final List<Record<? extends Hydratable>> records;
    private final UnrecognisedInputStrategy unrecognisedInputStrategy;

    private RecordHydrator(final Builder builder) {

        this.endianness = builder.endianness;

        this.records = builder.records.stream()
                .sorted(comparingInt(Record::getRecordWidth))
                .collect(toList());

        this.unrecognisedInputStrategy = builder.unrecognisedInputStrategy;
    }

    public HydrationResult hydrate(final BinaryDataInput input) {

        HydrationResult.Builder builder = HydrationResult.builder();

        InputData possibleRecord = readRecords(input);
        int processedBytes = processRecord(possibleRecord, builder);
        int unusedBytes = (possibleRecord.size() - processedBytes);

        while (input.hasInput()) {

            possibleRecord = readRecordsWithRemainder(input, possibleRecord, unusedBytes);
            processedBytes = processRecord(possibleRecord, builder);
            unusedBytes = (possibleRecord.size() - processedBytes) - 1;
        }

        return builder.build();
    }

    private int processRecord(final InputData possibleRecord,
                              final HydrationResult.Builder builder) {

        for (Record<?> record : records) {

            if (possibleRecord.size() >= record.getRecordWidth() && record.getSelector().isRecord(possibleRecord)) {

                Hydratable hydratableResult = record.deserialise(possibleRecord);

                builder.withResult(record.getType(), hydratableResult);

                return hydratableResult.getWidth();
            }
        }

        unrecognisedInputStrategy.processUnrecognisedInput(possibleRecord);

        return 0;
    }

    private InputData readRecordsWithRemainder(final BinaryDataInput input,
                                               final InputData inputData,
                                               final int remainder) {

        int currentRecordBytesRead = remainder;
        List<Short> inputValues = new ArrayList<>();

        for (int i = inputData.size() - remainder; i < inputData.size(); i++) {
            inputValues.add(inputData.get(i));
        }

        while (currentRecordBytesRead < getMaxRecordSize() && input.hasInput()) {

            Optional<Integer> value = input.read();

            if (!value.isPresent()) {
                break;
            }

            int result = value.get();
            inputValues.add((short) result);
            currentRecordBytesRead++;
        }

        return new InputData(endianness, inputValues);
    }

    private InputData readRecords(final BinaryDataInput input) {

        int currentRecordBytesRead = 0;
        List<Short> inputValues = new ArrayList<>();

        while (currentRecordBytesRead < getMaxRecordSize() && input.hasInput()) {

            Optional<Integer> value = input.read();

            if (!value.isPresent()) {
                break;
            }

            int result = value.get();
            inputValues.add((short) result);
            currentRecordBytesRead++;
        }

        if (currentRecordBytesRead < getMinRecordSize()) {
            throw new HydrationFailure(NOT_ENOUGH_INPUT, "Could not hydrate records as only '%s' bytes were contained "
                    + "in the input which is less than the minimum number of bytes required for the smallest "
                    + "registered record ('%s').", currentRecordBytesRead, getMinRecordSize());
        }

        return new InputData(endianness, inputValues);
    }

    private int getMaxRecordSize() {
        return records.stream()
                .mapToInt(Record::getRecordWidth)
                .max()
                .orElse(0);
    }

    private int getMinRecordSize() {
        return records.stream()
                .mapToInt(Record::getRecordWidth)
                .min()
                .orElse(0);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private ByteOrder endianness = BIG_ENDIAN;
        private final List<Record<?>> records = new ArrayList<>();
        private UnrecognisedInputStrategy unrecognisedInputStrategy = new FailOnUnrecognisedInput();

        private Builder() {

        }

        public <T extends Hydratable> Builder forIdentifier(final int recordOffset,
                                                            final short identifier,
                                                            final Class<T> type) {

            this.records.add(new Record<>(new ByByteIdentifier(recordOffset, identifier), type));
            return this;
        }

        public <T extends Hydratable> Builder forIdentifier(final Class<T> type,
                                                            final RecordSelector selector) {

            this.records.add(new Record<>(selector, type));
            return this;
        }

        public Builder withUnrecognisedInputStrategy(final UnrecognisedInputStrategy strategy) {
            this.unrecognisedInputStrategy = strategy;
            return this;
        }

        public Builder withByteOrder(final ByteOrder endianness) {
            this.endianness = endianness;
            return this;
        }

        public RecordHydrator build() {
            return new RecordHydrator(this);
        }
    }
}
