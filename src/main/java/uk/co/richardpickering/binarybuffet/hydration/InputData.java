package uk.co.richardpickering.binarybuffet.hydration;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import uk.co.richardpickering.binarybuffet.ByteOrder;
import uk.co.richardpickering.binarybuffet.hydration.extraction.ByteConcatenator;

import java.util.ArrayList;
import java.util.List;

public class InputData {

    private final ByteOrder endianness;
    private final List<Short> values = new ArrayList<>();

    public InputData(final ByteOrder endianness, final List<Short> values) {
        this.endianness = endianness;
        this.values.addAll(values);
    }

    public int size() {
        return values.size();
    }

    public short get(final int offset) {
        return values.get(offset);
    }

    public byte getByte(final int startIndex, final short width) {
        return (byte) ByteConcatenator.concatBytesToInt(endianness, startIndex, width, this);
    }

    public short getShort(final int startIndex, final short width) {
        return (short) ByteConcatenator.concatBytesToInt(endianness, startIndex, width, this);
    }

    public int getInteger(final int startIndex, final short width) {
        return (int) ByteConcatenator.concatBytesToInt(endianness, startIndex, width, this);
    }

    public long getLong(final int startIndex, final short width) {
        return ByteConcatenator.concatBytesToInt(endianness, startIndex, width, this);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InputData inputData = (InputData) o;
        return Objects.equal(values, inputData.values);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(values);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("values", values)
                .toString();
    }
}
