package uk.co.richardpickering.binarybuffet.hydration.instantiation;

import com.google.common.collect.ImmutableMap;
import uk.co.richardpickering.binarybuffet.FieldHydrationConversion;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Optional;

class UnboxedTypeDeserialiser {

    private static final String BYTE_NAME = "byte";
    private static final String SHORT_NAME = "short";
    private static final String INTEGER_NAME = "int";
    private static final String LONG_NAME = "long";

    private static final Map<String, FieldHydrationConversion<?>> CONVERSION_MAP =
            ImmutableMap.<String, FieldHydrationConversion<?>>builder()
                    .put(BYTE_NAME, (input, field) -> input.getByte(field.getOffset(), field.getWidth()))
                    .put(SHORT_NAME, (input, field) -> input.getShort(field.getOffset(), field.getWidth()))
                    .put(INTEGER_NAME, (input, field) -> input.getInteger(field.getOffset(), field.getWidth()))
                    .put(LONG_NAME, (input, field) -> input.getLong(field.getOffset(), field.getWidth()))
                    .build();

    Optional<FieldHydrationConversion<?>> getDeserialiser(final Field field) {
        return Optional.ofNullable(CONVERSION_MAP.get(field.getType().getName()));
    }
}
