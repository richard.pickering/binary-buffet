package uk.co.richardpickering.binarybuffet.hydration;

import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class HydrationResult {

    private Map<Class<?>, ResultList> records;

    private HydrationResult(final Builder builder) {
        this.records = builder.records;
    }

    public <T> List<T> getRecordsOfType(final Class<T> type) {
        return get(type)
                .map(ResultList::getResults)
                .map(list -> list.stream()
                        .map(type::cast)
                        .collect(toList()))
                .orElse(emptyList());
    }

    private <T> Optional<ResultList> get(final Class<T> type) {
        return Optional.ofNullable(records.get(type));
    }

    static Builder builder() {
        return new Builder();
    }

    private static class ResultList {
        private final List<Object> results = new ArrayList<>();

        private List<Object> getResults() {
            return results;
        }

        private ResultList add(final Object result) {
            results.add(result);
            return this;
        }
    }

    static class Builder {

        private final Map<Class<?>, ResultList> records = new LinkedHashMap<>();

        private Builder() {

        }

        public Builder withResult(final Class<?> type, final Object result) {
            this.records.put(type, getResults(type).add(result));
            return this;
        }

        public HydrationResult build() {
            return new HydrationResult(this);
        }

        private ResultList getResults(final Class<?> type) {
            return records.containsKey(type) ? records.get(type) : new ResultList();
        }
    }
}
