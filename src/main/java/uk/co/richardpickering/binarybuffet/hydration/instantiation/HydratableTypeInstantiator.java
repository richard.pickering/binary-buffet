package uk.co.richardpickering.binarybuffet.hydration.instantiation;

import uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static java.lang.reflect.Modifier.PUBLIC;
import static uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure.HydrationFailureType.HYDRATION_TARGET_INVALID;

public class HydratableTypeInstantiator {

    public HydratableTypeInstantiator() {

    }

    <T> T createInstance(final Class<T> type) {

        try {

            ensureTypeIsPublic(type);

            Constructor<T> constructor = getConstructor(type);

            constructor.setAccessible(true);

            return constructor.newInstance();

        } catch (final InstantiationException e) {
            throw new HydrationFailure(HYDRATION_TARGET_INVALID, e, "Could not hydrate as type '%s' is abstract.", type);
        } catch (final IllegalAccessException e) {
            throw new HydrationFailure(HYDRATION_TARGET_INVALID, e, "Could not hydrate as could not access default "
                    + "(parameterless) constructor of type '%s'", type);
        } catch (final InvocationTargetException e) {
            throw new HydrationFailure(HYDRATION_TARGET_INVALID, e, "Could not hydrate as exception thrown on "
                    + "default (parameterless) constructor invocation for class '%s'", type);
        }
    }

    private <T> void ensureTypeIsPublic(final Class<T> type) {

        if ((type.getModifiers() & PUBLIC) != PUBLIC) {
            throw new HydrationFailure(HYDRATION_TARGET_INVALID, "Could not hydrate as the class '%s' is not public.", type);
        }
    }

    private <T> Constructor<T> getConstructor(final Class<T> type) {

        try {

            return type.getDeclaredConstructor();

        } catch (final NoSuchMethodException e) {
            throw new HydrationFailure(HYDRATION_TARGET_INVALID, e, "Could not hydrate as there is no default "
                    + "(parameterless) constructor for type '%s'", type);
        }
    }
}
