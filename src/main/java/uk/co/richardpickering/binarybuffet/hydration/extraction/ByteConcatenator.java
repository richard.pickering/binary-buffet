package uk.co.richardpickering.binarybuffet.hydration.extraction;

import uk.co.richardpickering.binarybuffet.ByteOrder;
import uk.co.richardpickering.binarybuffet.hydration.InputData;

import static uk.co.richardpickering.binarybuffet.ByteOrder.LITTLE_ENDIAN;

public class ByteConcatenator {

    private static final short BITS_IN_BYTE = 8;

    public static long concatBytesToInt(final ByteOrder endianness,
                                        final int startIndex,
                                        final short width,
                                        final InputData input) {

        if (endianness == LITTLE_ENDIAN) {
            return concatBytesLittleEndian(startIndex, width, input);
        }

        return concatBytesBigEndian(startIndex, width, input);
    }

    private static long concatBytesBigEndian(final int startIndex,
                                             final short width,
                                             final InputData input) {

        int result = 0;

        for (int i = startIndex; i < startIndex + width; i++) {

            int relativeIndex = Math.abs((i - startIndex) - width) - 1;
            result += input.get(i) << (BITS_IN_BYTE * relativeIndex);
        }

        return result;
    }

    private static long concatBytesLittleEndian(final int startIndex,
                                                final short width,
                                                final InputData input) {

        int result = 0;

        for (int i = startIndex; i < startIndex + width; i++) {

            int relativeIndex = i - startIndex;
            result += input.get(i) << (BITS_IN_BYTE * relativeIndex);
        }

        return result;
    }
}
