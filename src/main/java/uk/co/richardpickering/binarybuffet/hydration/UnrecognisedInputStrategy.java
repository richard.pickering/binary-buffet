package uk.co.richardpickering.binarybuffet.hydration;

import uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure;

import static uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure.HydrationFailureType.UNRECOGNISED_INPUT;

public interface UnrecognisedInputStrategy {

    void processUnrecognisedInput(InputData unrecognisedInput);

    class FailOnUnrecognisedInput implements UnrecognisedInputStrategy {

        @Override
        public void processUnrecognisedInput(final InputData unrecognisedInput) {
            throw new HydrationFailure(UNRECOGNISED_INPUT, "Found unrecognised input data '%s'", unrecognisedInput);
        }
    }

    class IgnoreUnrecognisedInput implements UnrecognisedInputStrategy {

        @Override
        public void processUnrecognisedInput(final InputData unrecognisedInput) {

        }
    }
}
