package uk.co.richardpickering.binarybuffet.hydration.exception;

import static java.lang.String.format;

public class HydrationFailure extends RuntimeException {

    public enum HydrationFailureType {
        IO_ERROR,
        NOT_ENOUGH_INPUT,
        UNRECOGNISED_INPUT,
        HYDRATION_TARGET_INVALID
    }

    private final HydrationFailureType failureType;

    public HydrationFailure(final HydrationFailureType failureType,
                            final Exception cause,
                            final String message,
                            final Object... variables) {

        super(format("'%s': " + format(message, variables), failureType), cause);

        this.failureType = failureType;
    }

    public HydrationFailure(final HydrationFailureType failureType,
                            final String message,
                            final Object... variables) {

        super(format("'%s': " + format(message, variables), failureType));

        this.failureType = failureType;
    }

    public HydrationFailureType getFailureType() {
        return failureType;
    }
}
