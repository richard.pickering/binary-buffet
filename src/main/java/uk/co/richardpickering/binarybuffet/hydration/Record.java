package uk.co.richardpickering.binarybuffet.hydration;

import uk.co.richardpickering.binarybuffet.annotation.BinaryField;
import uk.co.richardpickering.binarybuffet.hydration.instantiation.BinaryFieldDeserialiser;
import uk.co.richardpickering.binarybuffet.hydration.instantiation.HydratableTypeDeserialiser;
import uk.co.richardpickering.binarybuffet.reflection.AnnotationExtractor;

/**
 * Represents a record to be 'hydrated' (deserialised) from some input bytes.
 * @param <T> The type to be 'hydrated'
 */
class Record<T extends Hydratable> {

    private final RecordSelector selector;
    private final Class<T> type;
    private final AnnotationExtractor annotationExtractor;
    private final HydratableTypeDeserialiser hydratableTypeDeserialiser;

    Record(final RecordSelector selector, final Class<T> type) {
        this.selector = selector;
        this.type = type;
        this.annotationExtractor = new AnnotationExtractor();
        this.hydratableTypeDeserialiser = new HydratableTypeDeserialiser(new BinaryFieldDeserialiser());
    }

    Class<T> getType() {
        return type;
    }

    RecordSelector getSelector() {
        return selector;
    }

    T deserialise(final InputData possibleRecord) {

        return hydratableTypeDeserialiser.deserialiseHydratable(type, possibleRecord);
    }

    int getRecordWidth() {
        return annotationExtractor.getFieldAnnotations(type, BinaryField.class)
                .values()
                .stream()
                .mapToInt(BinaryField::width)
                .sum();
    }
}
