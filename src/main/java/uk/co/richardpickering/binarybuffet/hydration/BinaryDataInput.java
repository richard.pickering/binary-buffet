package uk.co.richardpickering.binarybuffet.hydration;

import uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure.HydrationFailureType.IO_ERROR;

public interface BinaryDataInput {

    Optional<Integer> read();
    boolean hasInput();

    class InputStreamBinaryInput implements BinaryDataInput {

        private final InputStream inputStream;

        public InputStreamBinaryInput(final InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public Optional<Integer> read() {
            try {
                return Optional.of(inputStream.read())
                        .filter(data -> data >= 0);
            } catch (final IOException e) {
                throw new HydrationFailure(IO_ERROR, e, "Could not read from binary data input (input stream) as no more data.");
            }
        }

        @Override
        public boolean hasInput() {
            try {
                return inputStream.available() > 0;
            } catch (final IOException e) {
                throw new HydrationFailure(IO_ERROR, e, "Could not determine if the binary data input has any more bytes to read.");
            }
        }
    }
}
