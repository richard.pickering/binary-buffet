package uk.co.richardpickering.binarybuffet.hydration.instantiation;

import uk.co.richardpickering.binarybuffet.hydration.Hydratable;
import uk.co.richardpickering.binarybuffet.hydration.InputData;

public class HydratableTypeDeserialiser {

    private final BinaryFieldDeserialiser binaryFieldDeserialiser;
    private final HydratableTypeInstantiator typeInstantiator;

    public HydratableTypeDeserialiser(final BinaryFieldDeserialiser binaryFieldDeserialiser) {
        this.binaryFieldDeserialiser = binaryFieldDeserialiser;
        this.typeInstantiator = new HydratableTypeInstantiator();
    }

    public <T extends Hydratable> T deserialiseHydratable(final Class<T> type, final InputData possibleRecord) {

        T instance = typeInstantiator.createInstance(type);
        binaryFieldDeserialiser.deserialiseFields(type, possibleRecord, instance);
        return instance;
    }
}
