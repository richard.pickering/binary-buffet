package uk.co.richardpickering.binarybuffet.hydration.extraction;

import static com.google.common.base.Preconditions.checkState;

/**
 * Extracts a sub-byte value from the provided value.
 * For instance, if the value 10110011 were to be provided as the value
 * And we requested from bit 2 to bit 5, we would expect the following result:
 * 1100
 */
class SubByteValueExtractor {

    short extract(final byte fromBit, final byte toBit, final short value) {

        ensureRequestValid(fromBit, toBit);

        return (short) ((value & generateBitmask(fromBit, toBit)) >> fromBit);
    }

    private void ensureRequestValid(final byte fromBit, final byte toBit) {
        checkState(fromBit >= 0, "fromBit must be >= 0! Actual: '%s'", fromBit);
        checkState(fromBit <= 7, "fromBit must be <= 7! Actual: '%s'", fromBit);
        checkState(toBit > fromBit, "toBit must be >= fromBit! Actual toBit: '%s', fromBit: '%s'",
                fromBit, toBit);
        checkState(toBit <= 7, "toBit must be <= 7! Actual: '%s'", toBit);
    }

    private short generateBitmask(final byte fromBit, final byte toBit) {
        short bitmask = 0;

        for (int i = fromBit; i <= toBit; i++) {
            bitmask += (short) (1 << i);
        }

        return bitmask;
    }
}
