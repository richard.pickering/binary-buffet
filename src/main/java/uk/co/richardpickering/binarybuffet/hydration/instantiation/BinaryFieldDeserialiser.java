package uk.co.richardpickering.binarybuffet.hydration.instantiation;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.ClassUtils;
import uk.co.richardpickering.binarybuffet.FieldHydrationConversion;
import uk.co.richardpickering.binarybuffet.hydration.Hydratable;
import uk.co.richardpickering.binarybuffet.hydration.InputData;
import uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldData;
import uk.co.richardpickering.binarybuffet.reflection.BinaryFieldDataExtractor;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Optional;

import static uk.co.richardpickering.binarybuffet.hydration.exception.HydrationFailure.HydrationFailureType.HYDRATION_TARGET_INVALID;

public class BinaryFieldDeserialiser {

    private static final Map<Class<?>, FieldHydrationConversion<?>> CONVERSIONS =
            ImmutableMap.<Class<?>, FieldHydrationConversion<?>>builder()
                    .put(Byte.class, (input, field) -> input.getByte(field.getOffset(), field.getWidth()))
                    .put(Short.class, (input, field) -> input.getShort(field.getOffset(), field.getWidth()))
                    .put(Integer.class, (input, field) -> input.getInteger(field.getOffset(), field.getWidth()))
                    .put(Long.class, (input, field) -> input.getLong(field.getOffset(), field.getWidth()))
                    .put(String.class, (input, field) -> deserialiseStringField(field, input))
                    .build();

    private final BinaryFieldDataExtractor binaryFieldDataExtractor;
    private final UnboxedTypeDeserialiser unboxedTypeDeserialiser;

    public BinaryFieldDeserialiser() {
        this.binaryFieldDataExtractor = new BinaryFieldDataExtractor();
        this.unboxedTypeDeserialiser = new UnboxedTypeDeserialiser();
    }

    <T extends Hydratable> void deserialiseFields(final Class<T> type, final InputData possibleRecord, final T instance) {

        Map<Field, BinaryFieldData> fields = binaryFieldDataExtractor.extract(type);

        fields.forEach((field, binaryField) -> deserialiseField(field, binaryField, possibleRecord, instance));
    }

    private <T> void deserialiseField(final Field field,
                                      final BinaryFieldData binaryField,
                                      final InputData possibleRecord,
                                      final T instance) {

        field.setAccessible(true);

        FieldHydrationConversion<?> conversion = findHydrationForField(field, binaryField);

        Object value = conversion.convert(possibleRecord, binaryField);

        ensureConversionResultIsAssignable(field, value);

        setFieldValue(instance, field, value);
    }

    private FieldHydrationConversion<?> findHydrationForField(final Field field, final BinaryFieldData binaryField) {

        Optional<FieldHydrationConversion<?>> fieldLevelConversion = binaryField.getHydrationConversion();

        if (fieldLevelConversion.isPresent()) {
            return fieldLevelConversion.get();
        }

        Optional<FieldHydrationConversion<?>> conversion = Optional.ofNullable(CONVERSIONS.get(field.getType()));

        if (conversion.isPresent()) {
            return conversion.get();
        }

        conversion = unboxedTypeDeserialiser.getDeserialiser(field);

        if (conversion.isPresent()) {
            return conversion.get();
        }

        throw new HydrationFailure(HYDRATION_TARGET_INVALID, "No field level hydration conversion or hydration "
                + "field conversion registered for type '%s'", field.getType());
    }

    private void ensureConversionResultIsAssignable(final Field field, final Object value) {

        if (!ClassUtils.isAssignable(field.getType(), value.getClass(), true)) {
            throw new HydrationFailure(HYDRATION_TARGET_INVALID, "Cannot set field '%s' with value '%s' - type mismatch!",
                    field, value);
        }
    }

    private <T> void setFieldValue(final T instance, final Field field, final Object value) {

        try {

            field.set(instance, value);

        } catch (final IllegalAccessException e) {
            throw new HydrationFailure(HYDRATION_TARGET_INVALID, e, "Could not hydrate as could not access field "
                    + "'%s' for instance '%s' in order to set its value.", field, instance.getClass().getCanonicalName());
        }
    }

    private static String deserialiseStringField(final BinaryFieldData field, final InputData possibleRecord) {

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = field.getOffset(); i < field.getOffset() + field.getWidth(); i++) {

            char character = (char) possibleRecord.get(i);
            stringBuilder.append(character);
        }

        return stringBuilder.toString();
    }
}
