package uk.co.richardpickering.binarybuffet.hydration;

public interface Hydratable {

    int getWidth();
}
