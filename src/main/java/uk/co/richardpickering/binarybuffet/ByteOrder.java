package uk.co.richardpickering.binarybuffet;

public enum ByteOrder {
    BIG_ENDIAN,
    LITTLE_ENDIAN
}
