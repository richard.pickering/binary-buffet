package uk.co.richardpickering.binarybuffet;

import uk.co.richardpickering.binarybuffet.dessication.Dessicator;
import uk.co.richardpickering.binarybuffet.dessication.RecordDessicator;
import uk.co.richardpickering.binarybuffet.dessication.extraction.BinaryFieldExtractor;
import uk.co.richardpickering.binarybuffet.dessication.extraction.conversions.FieldConverter;
import uk.co.richardpickering.binarybuffet.hydration.RecordHydrator;

public class BinaryBuffet {

    public static Dessicator dessicator(final FieldConverter fieldConverter) {
        return new Dessicator(new RecordDessicator(new BinaryFieldExtractor(fieldConverter)));
    }

    public static RecordHydrator.Builder buildHydrator() {
        return RecordHydrator.builder();
    }
}
